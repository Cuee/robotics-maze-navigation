#!/usr/bin/env python
import rospy
from std_msgs.msg import String

def detect_obstacles():
    pub = rospy.Publisher('sensor_reading3', String, queue_size=10)
    rospy.init_node('ultra_sensor3', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    distance_measured = "10"
    while not rospy.is_shutdown():
	pub.publish(distance_measured)
	rate.sleep()
        
        

if __name__ == '__main__':
    try:
        detect_obstacles()
    except rospy.ROSInterruptException:
        pass
  

   
  
