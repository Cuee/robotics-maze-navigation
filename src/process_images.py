#!/usr/bin/env python
import rospy
from std_msgs.msg import String
import io
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import sys
sys.path.append('/usr/local/lib/python2.7/site-packages')
import cv2
import numpy as np
import roslib
import rospy
from sensor_msgs.msg import CompressedImage


time.sleep(0.0001)


class image_subscriber:

    def __init__(self):

        '''Initialize ros publisher, ros subscriber'''
        rospy.init_node('image_processor', anonymous=True)
        self.pub = rospy.Publisher('image_processing', String, queue_size=10)
        rospy.Subscriber("/raspicam_node/image/compressed", CompressedImage, self.callback,  queue_size = 15)
      
    
    def callback(self, ros_data, *args):
        
        np_arr = np.fromstring(ros_data.data, np.uint8)
        image = cv2.imdecode(np_arr, 1) 

        hsv = cv2.cvtColor(image,cv2.COLOR_BGR2HSV)
    	
        lower_blue = np.array([110,50,50])
        upper_blue = np.array([130,255,255])
     	
        lower_green = np.array([33,80,40])
        upper_green = np.array([102, 255, 255])
     
        lower_red = np.array([160,20,70])
        upper_red = np.array([190,255,255])	
    	
        lower_black = np.array([103, 86, 65])
        upper_black = np.array([145, 133, 128])

    	# I have the Green threshold image.
        green_mask = cv2.inRange(hsv, lower_green, upper_green) 

        # Threshold the HSV image to get only blue colors
        blue_mask = cv2.inRange(hsv, lower_blue, upper_blue)

        # Threshold the HSV image to get only blue colors
        red_mask = cv2.inRange(hsv, lower_red, upper_red)

        # Threshold the HSV image to get only blue colors
        black_mask = cv2.inRange(hsv, lower_black, upper_black)

        mask = blue_mask + green_mask  + red_mask + black_mask

    ###############################################
        if np.array_equal(mask, blue_mask):
            print("Blue found")
            self.pub.publish("blue")
            
    	
        elif( np.array_equal(mask, green_mask)):
            print("Green found")
            self.pub.publish("green")
    	
        elif (np.array_equal(mask, red_mask)):
            print("Red found")
            self.pub.publish("red")
    	
        elif (np.array_equal(mask, black_mask)):
            print("Black found")
            self.pub.publish("black")

        else:
    	    print("None")
            self.pub.publish("None")
            
    ################################################

        #cv2.imshow("Frame",image)
        #key = cv2.waitKey(1) & 0xFF


        #result = cv2.bitwise_and(image,image,mask=mask)
        #cv2.imshow("Result",result)
        #key = cv2.waitKey(1) & 0xFF

        #rawCapture.truncate(0)
        #if key == ord("q"):
         #       break

   
    
def main(args):
    '''Initializes and cleanup ros node'''
    ic = image_subscriber()
    # ic.calibrate()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        # print("Shutting down ROS Image feature detector module")
        pass
    cv2.destroyAllWindows()

if __name__ == '__main__':
    main(sys.argv)

  
    
    



