#!/usr/bin/env python
import RPi.GPIO as gpio
import rospy
from std_msgs.msg import String
import time

def init():
    gpio.setmode(gpio.BOARD)
    gpio.setup(7,gpio.OUT)
    gpio.setup(11,gpio.OUT)
    gpio.setup(13,gpio.OUT)
    gpio.setup(15,gpio.OUT)
     

def move_forward(tf):
    init()
    gpio.output(7, False)
    gpio.output(11, True)
    gpio.output(13,True)
    gpio.output(15, False)
    time.sleep(tf)
    gpio.cleanup()
  

def move_reverse(tf):
    init()
    gpio.output(7, True)
    gpio.output(11, False)
    gpio.output(13, False)
    gpio.output(15, True)
    time.sleep(tf)
    gpio.cleanup()
    
def turn_left(tf):
    init()
    gpio.output(7, True)
    gpio.output(11, True)
    gpio.output(13, True)
    gpio.output(15, False)
    time.sleep(tf)
    gpio.cleanup()

def turn_right(tf):
   init()
   gpio.output(7, False)
   gpio.output(11, False)
   gpio.output(13, False)
   gpio.output(15, True)
   time.sleep(tf)
   gpio.cleanup()


def callback(data):
    if data.data == "forward":
        forward()
    elif data.data == "left":
        left()
    elif data.data == "right":
        right()
    elif data.data == "backward":
        backward()
    else:
        rospy.loginfo("No Movement.")

def forward():
    rospy.loginfo(rospy.get_caller_id() + "Moving forward")
    move_forward(10)

def left():
    rospy.loginfo(rospy.get_caller_id() + "Moving left")
    turn_left(3)

def right():
    rospy.loginfo(rospy.get_caller_id() + "Moving right")
    turn_right(7)

def backward():
    rospy.loginfo(rospy.get_caller_id() + "Moving backward")
    move_reverse(5)


def subscriber():
    rospy.init_node('robot_movement', anonymous=True)
    rospy.Subscriber('path_to_take', String, callback)
    rospy.spin()

if __name__ == '__main__':
    subscriber()

