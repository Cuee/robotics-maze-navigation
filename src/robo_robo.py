#!/usr/bin/env python
import rospy
from std_msgs.msg import String

pub = rospy.Publisher('path_to_take', String, queue_size=15)    
sensor_reading1 = 0
sensor_reading2 = 0
sensor_reading3 = 0


def make_decision(color):
    if color == "red":
        pub.publish("left")
    elif color == "blue":
        pub.publish("right")
    elif color == "green":
        pub.publish("right")
    elif color == "black":
        pub.publish("backward")

def sensor_based_decision():
    global pub
    distances_measured = [sensor_reading1, sensor_reading2]
    farthest_path_from_obstacle = max(distances_measured)
    if farthest_path_from_obstacle == sensor_reading1:
        pub.publish("left")
    elif farthest_path_from_obstacle == sensor_reading2:
        pub.publish("right")
   
    
def callback_for_image(data):
    rospy.loginfo(rospy.get_caller_id() + 'color detected is: %s', data.data)
    if data.data == "None":
        sensor_based_decision()
    else:
        make_decision(data.data)
   
    
def callback_for_ultra_sensor1(data):
    rospy.loginfo(rospy.get_caller_id() + 'Distance measured by sensor 1 is: %s', data.data)
    global sensor_reading1
    sensor_reading1 = float(data.data)

def callback_for_ultra_sensor2(data):
    rospy.loginfo(rospy.get_caller_id() + 'Distance measured by sensor 2 is: %s', data.data)
    global sensor_reading2
    sensor_reading2 = float(data.data)

def subscriber():
    rospy.Subscriber('image_processing', String, callback_for_image)
    rospy.Subscriber('sensor_reading1', String, callback_for_ultra_sensor1) 
    rospy.Subscriber('sensor_reading2', String, callback_for_ultra_sensor2) 
    
    rospy.spin()
   

if __name__ == '__main__':
    rospy.init_node('decision_maker', anonymous=True)
    subscriber()
   

