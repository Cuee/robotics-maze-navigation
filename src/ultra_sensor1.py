#!/usr/bin/env python
import rospy
from std_msgs.msg import String
import RPi.GPIO as GPIO
import time

def detect_obstacles():
    
    pub = rospy.Publisher('sensor_reading1', String, queue_size=10)
    rospy.init_node('ultra_sensor1', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
      distance_measured = calculate_distance()
      pub.publish(str(distance_measured))
    rate.sleep()
        
GPIO.setmode(GPIO.BCM)

GPIOtrigger = 23
GPIOecho = 24
print("Distance measurment in progress")
GPIO.setup(GPIOtrigger, GPIO.OUT)
GPIO.setup(GPIOecho, GPIO.IN)

GPIO.output(GPIOtrigger, False)
print("Waiting for sensor to settle.")
time.sleep(2)

def calculate_distance():
      
    GPIO.output(GPIOtrigger, True)
    time.sleep(0.00001)
    GPIO.output(GPIOtrigger, False)

    while GPIO.input(GPIOecho) == 0:
      StartTime = time.time()
            
    while GPIO.input(GPIOecho) == 1:
      StopTime = time.time()
            
    TimeElapsed = StopTime - StartTime
    distance = TimeElapsed * 17150
    distance = round(distance, 2)
    rospy.loginfo(" Distance from obstacle is: ", distance, "cm")
    return distance
         
if __name__ == '__main__':
  try:
    detect_obstacles()
    GPIO.cleanup()
  except rospy.ROSInterruptException:
    pass

         
